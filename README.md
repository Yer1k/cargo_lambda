# Rust Lambda Function to Process Input Data and Return Output Data

[![CI/CD status](https://gitlab.com/Yer1k/cargo_lambda/badges/main/pipeline.svg)](https://gitlab.com/Yer1k/cargo_lambda/-/commits/main)


This project demonstrates a simple AWS Lambda function written in Rust using the `lambda_runtime` crate.

## Overview

The Lambda function takes an input data structure, processes it, and returns an output data structure.

- Input Data Structure: `InputData`
- Output Data Structure: `OutputData`

## Screenshots
![Invoke the Lambda function](https://gitlab.com/Yer1k/cargo_lambda/-/raw/main/screenshots/invoke_lambda.png)

## Deploy to AWS Lambda

[![Deploy to AWS](https://gitlab.com/Yer1k/cargo_lambda/-/raw/main/screenshots/deploy_to_aws.png)](https://console.aws.amazon.com/lambda/home?region=us-east-1#/create/app?applicationId=arn:aws:serverlessrepo:us-east-1:057560766410:applications/cargo-lambda)

> Note: Due to billing and security reasons, only IAM user can access the above link. You may need to deploy the lambda function to your AWS account manually.

## Requirements

- Rust (https://www.rust-lang.org/)
- `cargo-lambda` (https://www.cargo-lambda.info/guide/getting-started.html)
- AWS CLI configured with appropriate permissions

## Setup

1. Clone this repository:

    ```bash
    git clone git@gitlab.com:Yer1k/cargo_lambda.git
    ```

2. Navigate to the project directory:

    ```bash
    cd lambda_function
    ```

3. Build the Lambda function:

    ```bash
    cargo lambda build --release
    ```

4. Deploy the Lambda function to AWS:

    ```bash
    cargo lambda deploy --region <AWS_REGION> 
    ```

    Replace `<AWS_REGION>` with your desired AWS region.

## Usage

Once deployed, you can invoke the Lambda function using the AWS Management Console, AWS CLI, or AWS SDKs. The function expects input data in the format specified by the `InputData` struct and returns output data in the format specified by the `OutputData` struct.

## Testing

You can test the Lambda function locally using the `cargo-lambda` tool or by setting up a local development environment. For testing purposes, you can modify the `handler` function in `src/main.rs` to simulate different input scenarios and observe the output.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

