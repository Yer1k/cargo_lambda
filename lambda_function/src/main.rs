use lambda_runtime::{service_fn, Error};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct InputData {
    pub name: String,
    pub age: u32,
    // Add other fields as needed
}

#[derive(Debug, Deserialize, Serialize)]
pub struct OutputData {
    pub greeting: String,
    // Add other fields as needed
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: lambda_runtime::LambdaEvent<InputData>) -> Result<OutputData, Error> {
    // Extracting the input data from the LambdaEvent
    let input_data = event.payload;

    // Your data processing logic goes here
    let output_data = OutputData {
        greeting: format!(
            "Hello, {}! You are {} years old.",
            input_data.name, input_data.age
        ),
    };
    Ok(output_data)
}
